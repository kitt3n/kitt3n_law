<?php
namespace KITT3N\Kitt3nLaw\Controller;

/***
 *
 * This file is part of the "kitt3n | Law" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Oliver Merz <o.merz@kitt3n.de>
 *           Georg Kathan <g.kathan@kitt3n.de>
 *           Dominik Hilser <d.hilser@kitt3n.de>
 *
 ***/

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;


/**
 * CookieController
 */
class CookieController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * cookieRepository
     *
     * @var \KITT3N\Kitt3nLaw\Domain\Repository\CookieRepository
     * @inject
     */
    protected $cookieRepository = null;

    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {
        // Get all settings
        $aSettings = $this->settings;

        // Translated text snippets
        $sHeader = LocalizationUtility::translate('LLL:EXT:kitt3n_law/Resources/Private/Language/translation.xlf:header','kitt3n_law');
        $sMessage = LocalizationUtility::translate('LLL:EXT:kitt3n_law/Resources/Private/Language/translation.xlf:message','kitt3n_law');
        $sDismiss = LocalizationUtility::translate('LLL:EXT:kitt3n_law/Resources/Private/Language/translation.xlf:dismiss','kitt3n_law');
        $sAllow = LocalizationUtility::translate('LLL:EXT:kitt3n_law/Resources/Private/Language/translation.xlf:allow','kitt3n_law');
        $sLink = LocalizationUtility::translate('LLL:EXT:kitt3n_law/Resources/Private/Language/translation.xlf:link','kitt3n_law');

        // Define url of data protection
        if ($aSettings['cookieconsent']['privacy_uid']){
            $sPrivacyUrl = $this->uriBuilder->reset()
                ->setTargetPageUid($aSettings['cookieconsent']['privacy_uid'])
                ->setCreateAbsoluteUri(true)
                ->build();
        } else {
            $sPrivacyUrl = $aSettings['cookieconsent']['privacy_url'];
        }


        // Generate JavaScript to initialize CookieConsent
        $sJavaScriptBlock = '
<script type="text/javascript">
    function insertAfter(el, referenceNode) {
        referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
    }

    window.addEventListener("load", function(){
        window.cookieconsent.initialise({
            "cookie": {
                "name": "kitt3nlawcookieconsent",
            },
            "content": {
                "header": "' . $sHeader . '",
                "message": "' . $sMessage . '",
                "dismiss": "' . $sDismiss . '",
                "allow": "' . $sAllow . '",
                "link": "' . $sLink . '",
                '
            . ($sPrivacyUrl ? '"href": "' . $sPrivacyUrl . '"' : '') .
            '                   
            },
            "type": "opt-in",
            "layout": "basic-header",
            "position": "' . $aSettings['cookieconsent']['position'] . '",
            "theme": "' . ($aSettings['cookieconsent']['theme'] == 'wire' ? 'block' : $aSettings['cookieconsent']['theme']) . '",
            "palette": {
                "popup": {
                    "background": "' . $aSettings['cookieconsent']['popup_color'] . '",
                    ' . ($aSettings['cookieconsent']['popup_text'] ? '"text": "' . $aSettings['cookieconsent']['popup_text'] . '",' : '') . '
                    ' . ($aSettings['cookieconsent']['popup_link'] ? '"link": "' . $aSettings['cookieconsent']['popup_link'] . '"' : '') . '                       
                },
                "button": {                           
                    ' . ($aSettings['cookieconsent']['theme'] == 'wire' ? '"background": "transparent", "text": "' . $aSettings['cookieconsent']['button_color'] . '", "border": "' . $aSettings['cookieconsent']['button_color'] . '"' : '"background": "' . $aSettings['cookieconsent']['button_color'] . '",' . ($aSettings['cookieconsent']['button_text'] ? '"text": "' . $aSettings['cookieconsent']['button_text'] . '"' : '') ) . '                           
                }
            },
            "showLink": ' . ($aSettings['cookieconsent']['privacy_uid'] || $aSettings['cookieconsent']['privacy_url'] ? 'true' : 'false') . ',
            ' . ($aSettings['cookieconsent']['overlay'] ? '"onPopupOpen": function() { if (window.location.href != "' . $sPrivacyUrl . '") { var element = document.createElement("div"); element.className = "cc-overlay"; var ref = document.querySelector("div.cc-window"); insertAfter(element, ref); setTimeout(function () { element.classList.add("show"); }, 200);}}' : '') . '
           
        })
    });          
                
</script>';

        // compress JavaScript string
        $sJavaScriptBlock = preg_replace( "/\r|\n/", "", $sJavaScriptBlock );
        $sJavaScriptBlock = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $sJavaScriptBlock)));

        // pass script string to template
        $this->view->assign('sJavaScriptBlock', $sJavaScriptBlock);
    }
}