window.addEventListener("load", function () {

    var aCookieBtn = [];
    var oCookieBtn = document.getElementsByClassName('cc-btn');
    if (typeof oCookieBtn == "object" && oCookieBtn.length > 0) {
        for (var i = 0; i < oCookieBtn.length; i++) {
            (function (i) {
                aCookieBtn[i] = oCookieBtn[i];
                aCookieBtn[i].addEventListener("click", function( event ) {

                    var oOverlay = document.getElementsByClassName('cc-overlay');
                    var aOverlay = oOverlay;
                    aOverlay[0].classList.remove("show");
                    setTimeout(function() {
                        aOverlay[0].parentNode.removeChild(aOverlay[0]);
                    }, 300);

                }, false);
            })(i);
        }
    }


    var aCookieAllowBtn = [];
    var oCookieAllowBtn = document.getElementsByClassName('cc-allow');
    if (typeof oCookieAllowBtn == "object" && oCookieAllowBtn.length > 0) {
        for (var j = 0; j < oCookieAllowBtn.length; j++) {
            (function (j) {
                aCookieAllowBtn[j] = oCookieAllowBtn[j];
                aCookieAllowBtn[j].addEventListener("click", function(e) {

                    setTimeout(function() {
                        location.reload();
                    }, 500);

                }, false);
            })(j);
        }
    }
});