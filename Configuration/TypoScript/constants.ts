
plugin.tx_kitt3nlaw_kitt3nlawcookierender {
    view {
        # cat=plugin.tx_kitt3nlaw_kitt3nlawcookierender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:kitt3n_law/Resources/Private/Templates/
        # cat=plugin.tx_kitt3nlaw_kitt3nlawcookierender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:kitt3n_law/Resources/Private/Partials/
        # cat=plugin.tx_kitt3nlaw_kitt3nlawcookierender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:kitt3n_law/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_kitt3nlaw_kitt3nlawcookierender//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:kitt3n_law/Configuration/TypoScript/Constants" extensions="ts,txt">