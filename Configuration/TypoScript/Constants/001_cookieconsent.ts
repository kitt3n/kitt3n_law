plugin {
    tx_kitt3nlaw {
        settings {
            cookieconsent {
                # activate or deactivate cookieconsent
                active = 1

                # cookieconsent2 version number on https://cdnjs.cloudflare.com
                version = 3.0.6

                # Page uid of privacy policy (leave blank if not used)
                privacy_uid = 5

                # URL of privacy policy or other information (leave blank if not used)
                privacy_url =

                # Popup color
                popup_color = #f49700

                # Optional: Popup text color (leave blank if not used)
                popup_text = #ffffff

                # Optional: Popup link color (leave blank if not used)
                popup_link =

                # Button color
                button_color = #ffffff

                # Optional: Button text color (leave blank if not used)
                button_text = #f49700

                # Banner positions: top, bottom
                # Floating positions: top-left, top-right, bottom-left, bottom-right
                position = bottom-right

                # Available styles: block, edgeless, classic, wire
                theme = block

                # Overlay active or not (0 if not used)
                overlay = 1
            }
            tracking {
                # Activate Google Analytics Tracking (0 if not used)
                ga = 0
                # Google Analytics property ID
                ga_id = GA_TRACKING_ID

                # Activate Piwik/Matomo Tracking (0 if not used)
                pk = 0
                # Piwik/Matomo URL
                pk_url = https://piwik.zwei14.net/
                # Piwik/Matomo site ID
                pk_id = PK_TRACKING_ID

                # Activate eTracker Tracking (0 if not used)
                et = 0
                # eTracker configurations
                et_secure_code =
                et_pagename =
                et_areas =
                et_url =
                et_target =
                et_tval =
                et_tonr =
                et_tsale =
                et_basket =
                et_cust =
                et_easy =
                et_se =
            }
        }
    }

}